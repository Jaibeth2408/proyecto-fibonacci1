program fibonacci;

uses crt;

var
   opc:integer;
   contador:integer;
   numero,fibo1,fibo2,temp: integer;

BEGIN
    repeat
        clrscr;
        writeln('***********************************************');
        writeln('* Bienvenido al sistema de Sucesion Fibonacci *');
        writeln('*                                             *');
        writeln('* 1.Ver Informacion de la Sucesion Fibonacci  *');
        writeln('* 2.Calcular posicion Sucesion Fibonacci      *');
        writeln('* 3.Mostrar Posicion de la Sucesion Fibonacci *');
        writeln('* 4.Acerca del Sistema                        *');
        writeln('* 5.Salir                                     *');
        writeln('*                                             *');
        writeln('* Seleccione una opcion de menu               *');
        writeln('*                                             *');
        writeln('***********************************************');
        writeln('');
        readln(opc);
    if opc=1 then
        begin
          writeln('Informacion de la Sucesion Fibonacci');
          writeln('');
          writeln('¿Quien fue Fibonacci?');
          writeln('');
          writeln('Fibonacci fue un matematico italiano del siglo XIII, el primero en describir esta sucesión matematica.'); 
          writeln('Tambien se lo conocia como Leonardo de Pisa, Leonardo Pisano o Leonardo Bigollo y ya hablaba de la sucesion en el a#o 1202,');
          writeln('cuando publico su Liber abaci. Fibonacci era hijo de un comerciante y se crió viajando, en un medio en donde las matematicas');
          writeln('eran de gran importancia, despertando su interés en el cálculo de inmediato.');
          writeln('');
          writeln('Que es la La sucesión de Fibonacci');
          writeln('');
          writeln('Consta de una serie de números naturales que se suman de a 2, a partir de 0 y 1. Basicamente, la sucesion de Fibonacci se'); 
          writeln('realiza sumando siempre los últimos 2 números (Todos los numeros presentes en la sucesión se llaman numeros de Fibonacci'); 
          writeln('de la siguiente manera:');
          writeln('');
          writeln('0,1,1,2,3,5,8,13,21,34...');
          writeln('');
          writeln('(0+1=1 / 1+1=2 / 1+2=3 / 2+3=5 / 3+5=8 / 5+8=13 / 8+13=21 / 13+21=34...)');
          writeln('Así sucesivamente, hasta el infinito.');
          writeln('Por regla, la sucesión de Fibonacci se escribe asi: xn = xn-1 + xn-2.');
          writeln('');
          writeln('');
          writeln('Presione ENTER para ir al menu');
          readkey; 
         end;
         
    if opc=2 then
        begin
          writeln('Digite el numero para comprobar si esta en la Sucesion de fibonacci:');
          readln(numero);
          writeln('');
          fibo1:=0;
          fibo2:=1;
          temp:=fibo1+fibo2;
            if numero=1 then
                writeln(fibo1,' ');
            if numero=2 then
                writeln(fibo1, ' ', fibo2);
            if numero=3 then
                writeln(fibo1,' ', fibo2,' ',temp);
            if numero> 3 then
                Write(fibo1,' ',fibo2, ' ', temp);
                begin
                    repeat
                         if temp<numero then
                            begin
                                fibo1:=fibo2;
                                fibo2:=temp;
                                temp:=fibo1+fibo2;
                                
                                    if numero = temp then
                                        begin
                                            write(' ', temp);
                                            writeln;      
                                            writeln;
                                            writeln;
                                            writeln('*');
                                            writeln('El numero  ', numero, '   se encuentra dentro de la serie de Fibonacci');
                                            writeln('*');
                                    end
                        else
                            begin
                                write (' ', temp);
                            end;
                        end
                    until (temp=numero) or (temp>numero);
                        if temp>numero then
                            begin
                                writeln;      
                                writeln;
                                writeln;
                                writeln('*');
                                writeln('El numero   ', numero, '   No encuentra dentro de la serie de Fibonacci');
                                writeln('*');
                            end  
                        end;    
                
                
          writeln;      
          writeln;
          writeln('Presione ENTER para ir al menu');
          readkey; 
         end; 
    if opc=3 then
        begin
          writeln('Digite la posicion del numero de la Sucesion de fibonacci para mostrar:');
          readln(numero);
          writeln('');
          fibo1:=0;
          fibo2:=1;
          temp:=fibo1+fibo2;
          contador:=3;
          writeln;
          
            if numero=1 then
                writeln('La posicion:  ', numero, ' el numero es:  ', fibo1);
            if numero=2 then
                writeln('La posicion:  ', numero, ' el numero es:  ', fibo2);
            if numero=3 then
                writeln('La posicion:  ', numero, ' el numero es:  ', temp);
            
            if numero>3 then
            begin
                repeat
                    contador:=contador+1;
                    fibo1:=fibo2;
                    fibo2:=temp;
                    temp:=fibo1+fibo2;
                until contador=numero;
                writeln('La posicion:  ', numero, ' el numero es:  ',temp);
            end;    
          writeln;      
          writeln;
          writeln('Presione ENTER para ir al menu');
          readkey; 
         end;     
       if opc=4 then
        begin
          writeln('Realizado por:');
          writeln('Jaibeth Diaz');
          writeln('Ing en Sistemas - UNIMAR');
          writeln('Viernes, 04- Junio-2021');
          writeln('');
          writeln('Presione ENTER para ir al menu');
          readkey; 
         end;
    until opc=5; 
    writeln('Usted eligio salir del sistema');
    writeln('');
    writeln('Hasta Luego');
    writeln('');
    writeln('Presione una tecla para continuar');
    readkey;    
  END.


